///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  Joshua Brewer <brewerj3@hawaii.edu>
/// @date    24_Jan_2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define DEFAULT_MAX_NUMBER (2048)
int main( int argc, char* argv[] ) {
   int theMaxValue;
   if(argc!=1){
      theMaxValue=atoi(argv[1]);
      if(!(theMaxValue>=1)){
         printf("Please enter a valid integer\n");
         exit(1);
      }
   }
   else if(argc==1){
      theMaxValue = DEFAULT_MAX_NUMBER;
   }
   printf( "Cat `n Mouse\n" );
   //printf( "The number of arguments is: %d\n", argc );
   srand(time(NULL));
   int theNumberImThinkingOf = rand() % theMaxValue;
   int aGuess=0;
   printf("OK cat, I'm thinking of a number from 1 to %i. Make a guess:\n", theMaxValue);
   while(theNumberImThinkingOf != aGuess){
      scanf( "%d", &aGuess );
      if(aGuess<1){
         printf("You must enter a number that's >=1\n");
      }
      else if(aGuess>theMaxValue){
         printf("You must enter a number that's <=%i \n",theMaxValue);
      }
      else if(aGuess>theNumberImThinkingOf){
         printf("No cat... the number I'm thinking of is smaller than %i \n", aGuess);
      }
      else if(aGuess<theNumberImThinkingOf){
         printf("No cat... the number I'm thinking of is larger than %i \n", aGuess);
      }
   }
   printf("You got me \n |\\---/|\n | o_o |\n  \\_^_/\n");
   return 0;
}

